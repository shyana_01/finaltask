import requests

from api.helpers import PetRequestHelper


class PetsApiService:
    BASE_URL = 'https://petstore.swagger.io/v2/pet'

    def _generate_url(self, action_url):
        if action_url:
            return self.BASE_URL + action_url

        return self.BASE_URL

    def _get(self, action_url):
        url = self._generate_url(action_url)
        response = requests.get(url=url)
        return response

    def _post(self, body, action_url=None):
        url = self._generate_url(action_url)
        response = requests.post(url=url, json=body)
        return response.json()

    def _delete(self, action_url):
        url = self._generate_url(action_url)
        requests.delete(url=url)

    def add_pet(self, id, name, photoUrls=["string"]):
        body = PetRequestHelper.generate_body(id=id, name=name, photoUrls=photoUrls)
        self._post(body=body)

    def get_pet_by_id(self, id):
        url = f'/{id}'
        response = self._get(action_url=url)
        return response

    def delete_pet(self, id):
        url = f'/{id}'
        self._delete(action_url=url)
