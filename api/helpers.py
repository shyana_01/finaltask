class PetRequestHelper:
    @staticmethod
    def generate_body(id, name, photoUrls):
        return {
            "id": id,
            "name": name,
            "photoUrls": photoUrls
        }
