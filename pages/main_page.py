
from pages.base_page import BasePage


class MainPage(BasePage):
    URL = "http://localhost/litecart/en/"

    REGIONAL_SETTINGS_LOCATOR = "//a[text()='Regional Settings']"
    COUNTRY_LOCATOR = "//span[@class='select2-selection__arrow']"
    CURRENCY_LOCATOR = "//select[@name='currency_code']"
    CHOOSE_COUNTRY_LOCATOR = "//li[text()='{}']"
    CHOOSE_CURRENCY_LOCATOR = "//option[@value='{}']"
    SAVE_BUTTON_LOCATOR = "//button[@name='save']"
    CURRENCY_NAME_LOCATOR = "//div[@class='currency']"
    COUNTRY_NAME_LOCATOR = "//div[@class='country']"
    EMAIL_ADDRESS_INPUT_LOCATOR = "//input[@name='email']"
    PASSWORD_INPUT_LOCATOR = "//input[@name='password']"
    LOGIN_BUTTON_LOCATOR = "//button[@name='login']"
    LOGOUT_BUTTON_LOCATOR = "//a[text()='Logout']"
    DUCK_LOCATOR = "//a[contains(@href, '{}-duck-p')]"
    QUANTITY_INPUT_LOCATOR = "//input[@name='quantity']"
    ADD_TO_CART_BUTTON_LOCATOR = "//button[@name='add_cart_product']"
    CART_BUTTON_LOCATOR = "//a[@class='content']"
    QUANTITY_ITEM_LOCATOR = "//span[text()='{}']"

    def click_regional_settings_button(self):
        self.click(self.REGIONAL_SETTINGS_LOCATOR)

    def click_country(self):
        self.click(self.COUNTRY_LOCATOR)

    def click_currency(self):
        self.click(self.CURRENCY_LOCATOR)

    def choose_country(self, country):
        self.click(self.CHOOSE_COUNTRY_LOCATOR.format(country))

    def choose_currency(self, currency):
        self.click(self.CHOOSE_CURRENCY_LOCATOR.format(currency))

    def click_save_button(self):
        self.click(self.SAVE_BUTTON_LOCATOR)

    def update_settings(self, country=None, currency=None):
        self.click_regional_settings_button()

        if country is not None:
            self.click_country()
            self.choose_country(country)

        if currency is not None:
            self.click_currency()
            self.choose_currency(currency)

        self.click_save_button()

    def navigate(self):
        self.open_url(self.URL)

    def get_currency(self):
        return self.get_text(self.CURRENCY_NAME_LOCATOR)

    def get_country(self):
        return self.get_text(self.COUNTRY_NAME_LOCATOR)

    def enter_user_name(self, email):
        self.enter_text(self.EMAIL_ADDRESS_INPUT_LOCATOR, email)

    def enter_password(self, password):
        self.enter_text(self.PASSWORD_INPUT_LOCATOR, password)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def login(self, email, password):
        self.enter_user_name(email)
        self.enter_password(password)
        self.click_login_button()

    def is_logout_button_present(self):
        return self.is_element_present(self.LOGOUT_BUTTON_LOCATOR)

    def click_duck(self, color="blue"):
        self.click(self.DUCK_LOCATOR.format(color))

    def click_quantity_button(self):
        self.click(self.QUANTITY_INPUT_LOCATOR)

    def clear_quantity(self):
        self.clear_text(self.QUANTITY_INPUT_LOCATOR)

    def enter_quantity(self, number):
        self.enter_text(self.QUANTITY_INPUT_LOCATOR, number)

    def click_add_to_cart(self):
        self.click(self.ADD_TO_CART_BUTTON_LOCATOR)

    def click_cart_button(self):
        self.click(self.CART_BUTTON_LOCATOR)

    def add_ducks_to_cart(self, number):
        self.click_duck()
        self.click_quantity_button()
        self.clear_quantity()
        self.enter_quantity(number)
        self.click_add_to_cart()
        self.wait_for_element_present(self.QUANTITY_ITEM_LOCATOR.format(number))
        self.click_cart_button()
