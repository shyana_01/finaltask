from pages.base_page import BasePage


class CartPage(BasePage):
    QUANTITY_COLUMN_INDEX = 1
    COLUMN_VALUE_BY_ROW_COLUMN_INDEX_LOCATOR = "//table[@class='dataTable rounded-corners']//tr[{}]//td[{}]"

    TOTAL_PRICE_LOCATOR = "//td[@class='sum']"
    CONFIRM_ORDER_BUTTON_LOCATOR = "//button[@name='confirm_order']"
    REMOVE_BUTTON_LOCATOR = "//button[@name='remove_cart_item']"
    PRICE_OF_DUCK_LOCATOR = "//td[@class='unit-cost']"

    def get_quantity_of_duck_in_cart(self, quantity_row):
        locator = self.COLUMN_VALUE_BY_ROW_COLUMN_INDEX_LOCATOR.format(quantity_row, self.QUANTITY_COLUMN_INDEX)
        return self.get_text(locator)

    def _convert_price(self, price_string):
        price = float(price_string.replace("$", ""))
        return price

    def get_total_price(self):
        price_string =  self.get_text(self.TOTAL_PRICE_LOCATOR)
        price = self._convert_price(price_string)
        return price

    def click_remove_button(self):
        self.click(self.REMOVE_BUTTON_LOCATOR)

    def click_confirm_order_button(self):
        self.click(self.CONFIRM_ORDER_BUTTON_LOCATOR)

    def get_price_of_the_duck(self):
        price_string = self.get_text(self.PRICE_OF_DUCK_LOCATOR)
        price = self._convert_price(price_string)
        return price
