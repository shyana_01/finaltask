import string
import random

import pytest
from mysql.connector import connect
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from db.sql_order_service import SqlOrderService
from pages.cart_page import CartPage
from pages.main_page import MainPage


@pytest.fixture
def navigate_to_main_page(main_page):
    main_page.navigate()


@pytest.fixture
def chromedriver():
    chrome_driver = webdriver.Chrome(ChromeDriverManager().install())
    yield chrome_driver
    chrome_driver.quit()


@pytest.fixture
def main_page(chromedriver):
    return MainPage(chromedriver)


@pytest.fixture
def cart_page(chromedriver):
    return CartPage(chromedriver)


@pytest.fixture
def login(main_page):
    return main_page.login("shafaryana01@gmail.com", "Finaltask_1")


@pytest.fixture
def connection_to_db():
    connection = connect(
        host="localhost",
        user="root",
        database="litecart"
    )

    yield connection
    connection.close()


@pytest.fixture
def sql_order_service(connection_to_db):
    return SqlOrderService(connection_to_db)


@pytest.fixture
def attach_screenshot_for_failed(chromedriver, request):
    yield
    is_failed = request.session.testsfailed == 1

    if is_failed:
        random_str = ''.join(random.choice(string.ascii_letters) for i in range(3))
        chromedriver.save_screenshot(f"D:\Final_task_tms/screens/screen_{random_str}.png")
