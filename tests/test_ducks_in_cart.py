import allure
import pytest


@pytest.fixture
def setup_main_page_2(login, cart_page):
    yield
    cart_page.click_remove_button()


@pytest.mark.usefixtures("navigate_to_main_page", "setup_main_page_2", "attach_screenshot_for_failed")
class TestMainPage2:
    @allure.description("This test adds ducks to the basket and checks if they have been added")
    def test_ducks_in_cart(self, main_page, cart_page):
        number = '3'
        quantity_row_index = 2

        main_page.add_ducks_to_cart(number)
        quantity_value = cart_page.get_quantity_of_duck_in_cart(quantity_row_index)

        assert quantity_value == number, f"The number of ducks is not equal {number}"

    @allure.description("This test checks if the price is correct")
    def test_total_price(self, main_page, cart_page):
        number = 3
        main_page.add_ducks_to_cart(number)
        price = cart_page.get_price_of_the_duck()
        total_price = price * number

        assert cart_page.get_total_price() == total_price, f"The total price is not equal {total_price}"
        