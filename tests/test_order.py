import allure
import pytest


@pytest.mark.usefixtures("navigate_to_main_page", "login", "attach_screenshot_for_failed")
class TestOrder:
    @allure.description("This test checks if the order has been placed")
    def test_presence_of_order(self, main_page, cart_page, sql_order_service):
        number = 3

        initial_number_of_orders = sql_order_service.get_order_id_count()
        main_page.add_ducks_to_cart(number)
        cart_page.click_confirm_order_button()
        new_number_of_orders = sql_order_service.get_order_id_count()

        assert new_number_of_orders == initial_number_of_orders + 1, 'Order has not been placed'
