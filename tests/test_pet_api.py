import allure

from api.service import PetsApiService


class TestPetApi:
    @allure.description("This test adds animals to the cart and checks if they have been added")
    def test_add_pet(self):
        id = 4567
        name = 'Tom'

        PetsApiService().add_pet(id=id, name=name)
        response = PetsApiService().get_pet_by_id(id)
        pet = response.json()

        actual_result = {'id': pet.get('id'), 'name': pet.get('name')}
        expected_result = {'id': id, 'name': name}
        assert response.status_code == 200, f'Response status code is not 200'
        assert actual_result == expected_result, \
            f'Response contains information about wrong pet. Expected {expected_result}, but was {actual_result}'

    @allure.description("This test deletes animals and check if they have been deleted")
    def test_delete_pet(self):
        id = 4567

        PetsApiService().delete_pet(id=id)
        response = PetsApiService().get_pet_by_id(id)

        assert response.status_code == 404, \
            f'Expected 200 response code,but was: {response.status_code}. Pet was not deleted'
