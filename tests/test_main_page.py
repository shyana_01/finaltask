import allure
import pytest


@pytest.mark.usefixtures("navigate_to_main_page", "attach_screenshot_for_failed")
class TestMainPage:
    @allure.description("This test change country and currency and and checks if it has changed on ui")
    def test_change_regional_settings(self, main_page):
        country = 'Belarus'
        currency = 'USD'
        main_page.update_settings(country, currency)

        assert main_page.get_currency() == currency, f"Currency must be {currency}"
        assert main_page.get_country() == country, f"Country must be {country}"

    @allure.description("This test checks if the user is logged in")
    @pytest.mark.usefixtures("navigate_to_main_page", "login", "attach_screenshot_for_failed")
    def test_login(self, main_page):
        assert main_page.is_logout_button_present(), \
            "User cannot login with not valid credentials or user not registered"
