from db.sql_service import SqlService


class SqlOrderService(SqlService):
    def get_order_id_count(self):
        number_of_orders = self.execute_select_query("SELECT COUNT(id) FROM lc_orders")[0][0]
        return number_of_orders
