class SqlService:
    def __init__(self, connection):
        self.connection = connection
        self.cursor = self.connection.cursor()

    def execute_select_query(self, query):
        self.cursor.execute(query)
        data = self.cursor.fetchall()
        return data
